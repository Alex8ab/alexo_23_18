datagroup: default_datagroup {
  label: "Test Datagroup"
  description: "This is in Test Datagroup"
  sql_trigger:
              SELECT
              CASE WHEN EXTRACT(HOUR FROM CURRENT_TIMESTAMP) > 9
                AND EXTRACT(HOUR FROM CURRENT_TIMESTAMP) < 14
                THEN 'between 10am and 2pm'
                ELSE 'between 2pm and 10am'
                END;;
  max_cache_age: "1 hours"
}
