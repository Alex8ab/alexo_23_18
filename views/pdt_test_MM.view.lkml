view: pdt_test_mm {
  derived_table: {
    sql: SELECT
    inventory_items.id  AS `inventory_items.id`,
        (DATE(CONVERT_TZ(inventory_items.created_at ,'UTC','America/Los_Angeles'))) AS `inventory_items.created_date`,
        (DATE(CONVERT_TZ(inventory_items.sold_at ,'UTC','America/Los_Angeles'))) AS `inventory_items.sold_date`,
    inventory_items.cost  AS `inventory_items.cost`,
    users.email  AS `users.email`,
    users.age  AS `users.age`,
    users.city  AS `users.city`,
    orders.status  AS `orders.status`,
    order_items.phone  AS `order_items.phone`,
    order_items.sale_price  AS `order_items.sale_price`
    FROM demo_db.order_items  AS order_items
    LEFT JOIN demo_db.orders  AS orders ON order_items.order_id = orders.id
    LEFT JOIN demo_db.inventory_items  AS inventory_items ON order_items.inventory_item_id = inventory_items.id
    LEFT JOIN demo_db.users  AS users ON orders.user_id = users.id
    WHERE {% incrementcondition %} inventory_items {% endincrementcondition %};;
    sql_trigger_value: SELECT CURRENT_DATE ;;
  }

  dimension: id {
    description: ""
    type: number
  }
  dimension: created_date {
    description: ""
    type: date
  }
  dimension: sold_date {
    description: ""
    type: date
  }
  dimension: cost {
    description: ""
    type: number
  }
  dimension: email {
    description: ""
  }
  dimension: age {
    description: ""
    type: number
  }
  dimension: city {
    description: ""
  }
  dimension: status {
    description: ""
  }
  dimension: phone {
    description: ""
  }
  dimension: sale_price {
    description: ""
    type: number
  }

}
