view: hundred_million_orders {
  suggestions: yes
  sql_table_name: demo_db.hundred_million_orders ;;

  dimension: customer_id {
    full_suggestions: yes
    suggest_persist_for: "0 seconds"
    type: string
    sql: ${TABLE}.customer_id ;;
  }
  dimension: order_id {
    full_suggestions: yes
    suggest_persist_for: "0 seconds"
    type: number
    # hidden: yes
    sql: ${TABLE}.order_id ;;
  }
  dimension: order_price {
    full_suggestions: yes
    suggest_persist_for: "0 seconds"
    type: number
    sql: ${TABLE}.order_price ;;
  }
  measure: count {
    type: count
    drill_fields: [orders.id]
  }
}
