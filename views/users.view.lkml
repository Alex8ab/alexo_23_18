view: users {
  suggestions: yes
  sql_table_name: demo_db.users ;;
  drill_fields: [id]

  # Dummy change 27 Oct 2023
  ### changes v1

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: age {
    type: number
    sql: ${TABLE}.age ;;
  }
  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: city1 {
    type: string
    sql: ${TABLE}.city ;;
    link: {
      label: "Drill by City"
      url: "/explore/model_name/explore_name?fields=users.email,users.id&f[users.city]={{ value }}"
    }
  }
  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }
  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }
  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }
  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }
  # dimension: state {
  #   type: string
  #   sql: ${TABLE}.state ;;
  # }

  dimension: long_string{
    type: string
    sql: concat(${city},${country},${age},${last_name},${gender},${first_name},${long_string}) ;;

  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
    # group_label: "Test"
    # group_item_label: "Drill w/test"
    # drill_fields: [detail*]
    drill_fields: [ id, first_name, last_name, email ]

    link: {
      label: "Drill down test Jacob"
      url: "
      {% assign vis_config = '{
      \"show_view_names\":false,
      \"show_row_numbers\":false,
      \"size_to_fit\":true,
      \"type\":\"looker_grid\",
      \"series_labels\":{\"users.first_name\":\"Primer Nombre\",
                         \"users.last_name\":\"Apellido\",
                         \"users.email\":\"Correo\"},
      \"header_font_size\":\"14\",
      \"rows_font_size\":\"12\"
      }' %}
      {{ link }}
      &vis_config={{ vis_config | encode_uri }}
      &fields=users.first_name,
              users.last_name,
              users.email
      &pivots=users.country
      &sorts=users.id+asc
      &limit=50
      &column_limit=50
      &toggle=dat,pik,vis
      "
    }
  }

  #   link: {
  #     label: "Drill to Basic KPIs Detail"
  #     url: "
  #     {% assign vis_config = '{
  #     \"show_view_names\":false,
  #     \"show_row_numbers\":false,
  #     \"transpose\":false,
  #     \"truncate_text\":false,
  #     \"hide_totals\":false,
  #     \"hide_row_totals\":false,
  #     \"size_to_fit\":true,
  #     \"table_theme\":\"white\",
  #     \"limit_displayed_rows\":false,
  #     \"enable_conditional_formatting\":true,
  #     \"header_text_alignment\":\"left\",
  #     \"header_font_size\":\"12\",
  #     \"rows_font_size\":\"12\",
  #     \"conditional_formatting_include_totals\":false,
  #     \"conditional_formatting_include_nulls\":false,
  #     \"color_application\":{\"collection_id\":\"cni\",
  #                           \"palette_id\":\"cni-categorical-0\"},
  #     \"show_sql_query_menu_options\":false,
  #     \"show_totals\":true,
  #     \"show_row_totals\":true,
  #     \"truncate_header\":false,
  #     \"series_labels\":{\"users.first_name\":\"Primer Nombre\",
  #                       \"users.last_name\":\"Apellido\",
  #                       \"users.email\":\"Correo\"},
  #     \"series_column_widths\":{\"users.id\":120},
  #     \"series_cell_visualizations\":{\"users.id\":{\"is_active\":false}},
  #     \"type\":\"looker_grid\",
  #     \"defaults_version\":1,
  #     \"series_types\":{}

  #     }' %}

  #     {{ link }}
  #     &vis_config={{ vis_config | encode_uri }}
  #     &fields=users.first_name,
  #             users.last_name,
  #             users.email
  #     &sorts=users.id+asc
  #     &limit=50
  #     &column_limit=50
  #     &toggle=dat,pik,vis
  #     "
  #   }
  # }


  dimension: zip {
    type: zipcode
    sql: ${TABLE}.zip ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
  id,
  first_name,
  last_name,
  email
  ]
  }

}
