view: circular {
    derived_table: {
      explore_source: order_items {
        bind_all_filters: yes
        column: id {}
        column: count {}
        column: age { field: users.age }
        column: city { field: users.city }
        column: country { field: users.country }
      }
    }
    dimension: id {
      description: ""
      type: number
    }
    dimension: count {
      description: ""
      type: number
    }
    dimension: age {
      description: ""
      type: number
    }
    dimension: city {
      description: ""
    }
    dimension: country {
      description: ""
    }
  }

