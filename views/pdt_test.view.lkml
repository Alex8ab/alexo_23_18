
view: pdt_test {
  derived_table: {
    datagroup_trigger: default_datagroup
    explore_source: order_items {
      column: id {}
      column: sale_price {}
      column: count {}
    }
  }
  dimension: id {
    description: ""
    primary_key: yes
    type: number
  }
  dimension: sale_price {
    description: ""
    type: number
  }
  dimension: count {
    description: ""
    type: number
  }
}
