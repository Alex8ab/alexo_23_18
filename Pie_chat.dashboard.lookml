---
- dashboard: pie_chat
  title: Pie chat
  layout: newspaper
  preferred_viewer: dashboards-next
  preferred_slug: zFLntFxACoA9edKIX3N9PC
  elements:
  - title: Pie chat
    name: Pie chat
    model: alexo_23_16
    explore: order_items
    type: looker_pie
    fields: [order_items.count, orders.id]
    sorts: [order_items.count desc 0]
    limit: 50
    column_limit: 50
    query_timezone: America/Los_Angeles
    value_labels: legend
    label_type: labPer
    show_value_labels: true
    font_size: 12
    defaults_version: 1
    hidden_pivots: {}
    listen: {}
    row: 0
    col: 0
    width: 24
    height: 12
