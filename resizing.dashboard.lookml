---
- dashboard: resizing
  title: resizing
  layout: newspaper
  preferred_viewer: dashboards-next
  preferred_slug: ojudLkY6LgivVVnqxwZwQj
  elements:
  - title: resizing
    name: resizing
    model: alexo_23_16
    explore: order_items
    type: looker_donut_multiples
    fields: [order_items.count, orders.count, products.count, users.count]
    limit: 500
    column_limit: 50
    arm_length: 25
    arm_weight: 25
    spinner_length: 25
    spinner_weight: 25
    angle: 90
    cutout: 50
    query_timezone: America/Los_Angeles
    show_value_labels: true
    font_size: 12
    defaults_version: 1
    listen: {}
    row: 0
    col: 0
    width: 10
    height: 10
